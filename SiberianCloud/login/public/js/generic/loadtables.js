  $(document).ready(function() {
    $('a[data-toggle="tab"]').on( 'shown.bs.tab', function (e) {
        $.fn.dataTable.tables( {visible: true, api: true} ).columns.adjust();
    } );   
    
} );

   //======INICIO DE FUNCIONES ============
   function LoadTableFamily()
   {   

      var table = $("#TableEmployeeFamily").DataTable({
        //COMPROBACION PARA PINTAR Y CAMBIAR TEXTO DE TABLA
        "destroy": true,
        "scrollY":        "80px",
        "scrollX": "1100px",
        "paging":   false,
        
        "info":     false,
        "searching": false,
        "language": {
            "url": "/../Spanish.json"
        },
        //Especificaciones de las Columnas que vienen y deben mostrarse
        "columns" : [
        { data : 'idn' },
        { data : 'typefamily'},
        { data : 'firstname' },
        { data : 'lastname' },
        { data : 'gender' }

        ],
        //Especificaciones de la URL del servicio
        "ajax": {
            url: '../api/v1/employee/families/'+empDP.fd.Idn,
            dataSrc : ''
        }
    });
      $('#TableEmployeeFamily tbody').on( 'click', 'tr', function () {

        var row = table.row( this ).data();
        empFa.fd.Idn = row.idn;
        empFa.fd.Familytype = row.typefamily;
        empFa.fd.Firstname = row.firstname;
        empFa.fd.Lastname = row.lastname;
        empFa.fd.Cuil = row.cuil;
        empFa.fd.Birthdate = FormatDate(row.birthdate);
        empFa.fd.Gender = row.gender;      
        empFa.openmodaltoedit();

    });

  }

  function LoadTableHours()
  {

      var table = $("#TableHoursworked").DataTable({
          //COMPROBACION PARA PINTAR Y CAMBIAR TEXTO DE TABLA
          "destroy": true,
          "scrollY":        "80px",
          "scrollX": "1100px",
          "paging":   false,

          "info":     false,
          "searching": false,
          "language": {
              "url": "/../Spanish.json"
          },
          //Especificaciones de las Columnas que vienen y deben mostrarse
          "columns" : [
              { data : 'idn' },
              { data : 'startdate'},
              { data : 'finishdate' },
              { data : 'starthour' },
              { data : 'finishhour' },
              { data : 'cant' },
              { data : 'observation' }

          ],
          //Especificaciones de la URL del servicio
          "ajax": {
              url: '../api/v1/employee/hoursworked/'+empDP.fd.Idn,
              dataSrc : ''
          }
      });
      $('#TableHoursworked tbody').on( 'click', 'tr', function () {

          var row = table.row( this ).data();
          empHours.fd.Idn = row.idn;
          empHours.fd.Startdate = FormatDate(row.startdate);
          empHours.fd.Finishdate = FormatDate(row.finishdate);
          empHours.fd.StartHour = row.starthour;
          empHours.fd.FinishHour = row.finishhour;
          empHours.fd.Cant = row.cant;
          empHours.fd.Observation = row.observation;
          empHours.openmodaltoedit();

      });

  }
  function LoadTableBankAcount()
  {   

      var table = $("#TableEmployeeBankAccount").DataTable({
        //COMPROBACION PARA PINTAR Y CAMBIAR TEXTO DE TABLA
        "destroy": true,
        "scrollY":        "100px",
        "scrollX": "1100px",
        "paging":   false,        
        "info":     false,
        "searching": false,

        "language": {
            "url": "/../Spanish.json"
        },
        //Especificaciones de las Columnas que vienen y deben mostrarse
        "columns" : [
        { data : 'idn' },
        { data : 'paymentmethod'},      
        { data : 'namebank' },
        { data : 'numberaccount' }

        ],
        //Especificaciones de la URL del servicio
        "ajax": {
            url: '../api/v1/employee/bankaccounts/'+empDP.fd.Idn,
            dataSrc : ''
        }
    });
      $('#TableEmployeeBankAccount tbody').on( 'click', 'tr', function () {

        var row = table.row( this ).data();
        empBa.fd.Idn = row.idn;
        empBa.fd.Paymentmethod = row.idnpaymentmethod;
        empBa.fd.Accounttype = row.accountype;
        empBa.fd.Bank = row.idnbank;
        empBa.fd.Numberaccount = row.numberaccount;
        empBa.fd.Cbu = row.cbu;
        empBa.fd.Branch = row.branch;
        empBa.fd.Percentage = row.percentage;

        empBa.openmodaltoedit();


    });

  } 
  function LoadTableCareer()
  {   

      var table = $("#TableEmployeeCareer").DataTable({
        //COMPROBACION PARA PINTAR Y CAMBIAR TEXTO DE TABLA
        "destroy": true,
        "scrollY":        "100px",
        "scrollX": "1100px",
        "paging":   false,      
        "info":     false,
        "searching": false,

        "language": {
            "url": "/../Spanish.json"
        },
        //Especificaciones de las Columnas que vienen y deben mostrarse
        "columns" : [
        { data : 'idn' },
        { data : 'namecareer'},
        { data : 'title' },
        { data : 'startdate' },
        { data : 'finishdate' }

        ],
        //Especificaciones de la URL del servicio
        "ajax": {
            url: '../api/v1/employee/careers/'+empDP.fd.Idn,
            dataSrc : ''
        }
    });
      $('#TableEmployeeCareer tbody').on( 'click', 'tr', function () {

        var row = table.row( this ).data();
        
        empCa.fd.Idn = row.idn;
        empCa.fd.Career = getactualvalue(empCa.fd.Careerlist,row.idncareer);    
        empCa.fd.StartDate = FormatDate(row.startdate); 
        empCa.fd.FinishDate = FormatDate(row.finishdate); 
        empCa.fd.Title = row.title;       
        empCa.openmodaltoedit();

    });

  } 
  function LoadTableLanguage()
  {   

      var table = $("#TableEmployeeLanguage").DataTable({
        //COMPROBACION PARA PINTAR Y CAMBIAR TEXTO DE TABLA
        "destroy": true,
        "scrollY":        "100px",
        "scrollX": "900px",
        "paging":   false,
        
        "info":     false,
        "searching": false,

        "language": {
            "url": "/../Spanish.json"
        },
        //Especificaciones de las Columnas que vienen y deben mostrarse
        "columns" : [
        { data : 'idn' },
        { data : 'namelanguage'},
        { data : 'level' }


        ],
        //Especificaciones de la URL del servicio
        "ajax": {
            url: '../api/v1/employee/languages/'+empDP.fd.Idn,
            dataSrc : ''
        }
    });
      $('#TableEmployeeLanguage tbody').on( 'click', 'tr', function () {

        var row = table.row( this ).data();
        
        empLa.fd.Idn = row.idn;
        empLa.fd.Language = getactualvalue(empLa.fd.Languagelist,row.idnlanguage);      
        empLa.fd.Level = row.level;  
        empLa.openmodaltoedit();
    });

  } 
  function LoadTableGroupi()
  {   
      var table = $("#TableEmployeeGroup").DataTable({
        //COMPROBACION PARA PINTAR Y CAMBIAR TEXTO DE TABLA
        "destroy": true,
        "scrollY": "100px",
        "scrollX": "900px",
        "language": {
            "url": "/../Spanish.json"
        },
        //Especificaciones de las Columnas que vienen y deben mostrarse
        "columns" : [
        { data : 'idn' },       
        { data : 'namegroup'}
        ],
        //Especificaciones de la URL del servicio
        "ajax": {
            url: '../api/v1/employee/groups/'+empDP.fd.Idn+'',
            dataSrc : ''
        }
    });
      $('#TableEmployeeGroup tbody').on( 'click', 'tr', function () {

        var row = table.row( this ).data();
        empGroup.fd.Idn = row.idn;
        empGroup.fd.Employee = row.idnemployee;
        empGroup.fd.Group = getactualvalue(empGroup.fd.Grouplist,row.idngroup);
        empGroup.fd.Name = row.namegroup;
        empGroup.openmodaltoedit();
    });
  } 
  function LoadTableAsignationdeductions()
  {   
      var table = $("#TableAsigDeduc").DataTable({
        //COMPROBACION PARA PINTAR Y CAMBIAR TEXTO DE TABLA
        "destroy": true,
        "scrollY": "100px",
        "scrollX": "900px",
        "language": {
            "url": "../Spanish.json"
        },
        //Especificaciones de las Columnas que vienen y deben mostrarse
        "columns" : [
        { data : 'idn' },
        { data : 'Period'},
        { data : 'Concept'},
        { data : 'Typerun'},
        { data : 'cant'},
        { data : 'amount'},
        { data : 'startdate'},
        { data : 'finishdate'}
        ],
        //Especificaciones de la URL del servicio
        "ajax": {
            url: '../api/v1/employee/asignationdeductions/'+empDP.fd.Idn, //+empGroupi.fd.Idn
            dataSrc : ''
        }
    });
      $('#TableAsigDeduc tbody').on( 'click', 'tr', function () {

        var row = table.row( this ).data();
        empAsig.fd.Idn = row.idn;
        empAsig.fd.Period = getactualvalue(empAsig.fd.Periodlist,row.idnperiod);
        empAsig.fd.Concept = getactualvalue(empAsig.fd.Conceptlist,row.idnconcept);
        empAsig.fd.Typerun = getactualvalue(empAsig.fd.Typerunlist,row.idntyperun);
        empAsig.fd.Amount = row.amount;
        empAsig.fd.Cant = row.cant;
        empAsig.fd.Specialtype = getactualvalue(empAsig.fd.Specialtypelist,row.specialtype);
        empAsig.fd.Description = row.description;
        empAsig.fd.Startdate = FormatDate(row.startdate);
        empAsig.fd.Finishdate = FormatDate(row.finishdate);
        empAsig.openmodaltoedit();

    });

  } 


  function LoadTableEmployeeAbsences()
  {   


      var table = $("#TableEmployeeAbsences").DataTable({
        //COMPROBACION PARA PINTAR Y CAMBIAR TEXTO DE TABLA
        "destroy": true,
        "scrollY": "100px",
        "scrollX": "900px",

        "language": {
            "url": "../Spanish.json"
        },
        //Especificaciones de las Columnas que vienen y deben mostrarse
        "columns" : [
        { data : 'idn' },
        { data : 'nameabsencetype'},
        { data : 'cant'},
        { data : 'startdate'},
        { data : 'finishdate'} 



        ],
        //Especificaciones de la URL del servicio
        "ajax": {
            url: '../api/v1/employee/absence/'+empDP.fd.Idn, //+empGroupi.fd.Idn
            dataSrc : ''
        }
    });
      $('#TableEmployeeAbsences tbody').on( 'click', 'tr', function () {

        var row = table.row( this ).data();
        empAbsen.fd.Idn = row.idn;
        empAbsen.fd.AbsenceType = getactualvalue(empAbsen.fd.AbsenceTypelist,row.idnabsencetype);
        empAbsen.fd.Startdate = FormatDate(row.startdate);
        empAbsen.fd.Finishdate = FormatDate(row.finishdate);
        empAbsen.fd.Cant = row.cant;
        
        empAbsen.fd.Observation = row.observation;
        
        empAbsen.openmodaltoedit();

    });

  } 
  function LoadTableEmployeeExclusions()
  {   


      var table = $("#TableExclusions").DataTable({
        //COMPROBACION PARA PINTAR Y CAMBIAR TEXTO DE TABLA
        "destroy": true,
        "scrollY": "100px",
        "scrollX": "900px",      
        
        "language": {
            "url": "../Spanish.json"
        },
        //Especificaciones de las Columnas que vienen y deben mostrarse
        "columns" : [
        { data : 'idn' },
        { data : 'nameperiod'},
        { data : 'typerun'},
        { data : 'nameconcept'},     



        ],
        //Especificaciones de la URL del servicio
        "ajax": {
            url: '../api/v1/employee/exclusion/'+empDP.fd.Idn, //+empGroupi.fd.Idn
            dataSrc : ''
        }
    });
      $('#TableExclusions tbody').on( 'click', 'tr', function () {

        var row = table.row( this ).data();
        empExclu.fd.Idn = row.idn;
        
        empExclu.fd.Period = getactualvalue(empExclu.fd.Periodlist,row.idnperiod);
        empExclu.fd.Typerun = getactualvalue(empExclu.fd.Typerunlist,row.idntyperun);        
        empExclu.fd.Concept = getactualvalue(empExclu.fd.Conceptlist,row.idnconcept);
        
        empExclu.openmodaltoedit();

    });

  } 
  $(window).bind('resize', function () {
    table.fnAdjustColumnSizing();
} );
  function LoadTableEmployeeVacations()
  {   


      var table = $("#TableEmployeeVacations").DataTable({
        //COMPROBACION PARA PINTAR Y CAMBIAR TEXTO DE TABLA
        "destroy": true,
        "scrollY": "100px",
        "scrollX": "900px",

        "language": {
            "url": "../Spanish.json"
        },
        //Especificaciones de las Columnas que vienen y deben mostrarse
        "columns" : [
        { data : 'idn' },
        { data : 'nameperiod'},
        { data : 'typerun'},
        { data : 'startdate'},
        { data : 'finishdate'},
        { data : 'year'},
        { data : 'cant'}


        ],
        //Especificaciones de la URL del servicio
        "ajax": {
            url: '../api/v1/employee/vacation/'+empDP.fd.Idn, //+empGroupi.fd.Idn
            dataSrc : ''
        }
    });
      $('#TableEmployeeVacations tbody').on( 'click', 'tr', function () {

        var row = table.row( this ).data();
        empVaca.fd.Idn = row.idn;
        empVaca.fd.Period = getactualvalue(empVaca.fd.Periodlist,row.idnperiod);
        empVaca.fd.Typerun = getactualvalue(empVaca.fd.Typerunlist,row.idntyperun);
        empVaca.fd.Startdate = FormatDate(row.startdate);
        empVaca.fd.Finishdate = FormatDate(row.finishdate);
        empVaca.fd.Year = row.year;
        
        empVaca.fd.Cant = row.cant;
        
        empVaca.openmodaltoedit();

    });

  } 

  function LoadTableEmployeeConstants()
  {   


      var table = $("#TableEmployeeConstant").DataTable({
        //COMPROBACION PARA PINTAR Y CAMBIAR TEXTO DE TABLA
        "destroy": true,
        "scrollY": "100px",
        "scrollX": "900px",

        "language": {
            "url": "../Spanish.json"
        },
        //Especificaciones de las Columnas que vienen y deben mostrarse
        "columns" : [
        { data : 'idn' },
        { data : 'periodname'},
        { data : 'typerun'},
        { data : 'constantcod'},
        { data : 'value'},
        { data : 'comment'}
        


        ],
        //Especificaciones de la URL del servicio
        "ajax": {
            url: '../api/v1/employee/constant/'+empDP.fd.Idn, //+empGroupi.fd.Idn
            dataSrc : ''
        }
    });
      $('#TableEmployeeConstant tbody').on( 'click', 'tr', function () {

        var row = table.row( this ).data();
        empConst.fd.Idn = row.idn;
        empConst.fd.Period = getactualvalue(empConst.fd.Periodlist,row.idnperiod);
        empConst.fd.Typerun = getactualvalue(empConst.fd.Typerunlist,row.idnperiod);      
        empConst.fd.Constant = getactualvalue(empConst.fd.Constantlist,row.idnconstant);
        empConst.fd.Value = row.value;
        empConst.fd.Comment = row.comment;
        
        empConst.openmodaltoedit();

    });

  } 



//CARGAR TABLA DE CORRIDA ABIERTA SEGUN EMPLEADO
function LoadTablePayrunOpenByEmployee()
{   

  var table = $("#TablePayrunOpenByEmployee").DataTable({
        //COMPROBACION PARA PINTAR Y CAMBIAR TEXTO DE TABLA
        "destroy": true,
        "scrollY":        "400px",
        "scrollX": "1100px",

        "language": {
            "url": "../Spanish.json"
        },
        //Especificaciones de las Columnas que vienen y deben mostrarse
        "columns" : [
        { data : 'idn' },
        { data : 'nametyperun'},
        { data : 'nameroster'},
        { data : 'titleperiod'},
        { data : 'payments' },
        { data : 'unremuns' },
        { data : 'deductions' },
        { data : 'totalpay' }


        ],
        //Especificaciones de la URL del servicio
        "ajax": {
            url: '../api/v1/employee/payrunhead/open/'+empDP.fd.Idn,
            dataSrc : ''
        }
    });
  $('#TablePayrunOpenByEmployee tbody').on( 'click', 'tr', function () {

        var row = table.row( this ).data();
        empRun.fd.DetPayrunHead = row.idn;
        empRun.fd.DetTyperun = row.idntyperun;
        empRun.fd.DetPeriod = row.idnperiod;
        empRun.fd.DetNamePeriod = row.titleperiod;
        empRun.fd.DetNameTyperun = row.nametyperun;
        empRun.fd.DetEmployee = row.idnemployee;
        empRun.fd.DetRoster = row.idnroster;
        empRun.fd.DetPaymentmethod = row.idnpaymentmethod;
        empRun.fd.DetPaymentRundate = row.paydate;
        empRun.fd.DetPayContributiondate = row.paycontributiondate;
        console.log('Selected - Payrunhead:'+empRun.fd.DetPayrunHead);
        console.log('Selected - Typerun:'+empRun.fd.DetTyperun);
        console.log('Selected - Period:'+empRun.fd.DetPeriod);
        console.log('Selected - Employee:'+empRun.fd.DetEmployee);
        console.log('Selected - Roster:'+empRun.fd.DetRoster);
        console.log('Selected - Payment Method:'+empRun.fd.DetPaymentmethod);
        console.log('Selected - Payment RunDate:'+empRun.fd.DetPaymentRundate);
        console.log('Selected - Contribution Date:'+empRun.fd.DetPayContributiondate);
        

        empRun.openpayrundetail();

        //TRANSFERENCIA DE DATOS HACIA LA MODAL DE DETALLE
        empRun.fd.HeadCod = empHead.fd.HeadCod;
        empRun.fd.HeadName = empHead.fd.HeadName;
        empRun.fd.HeadCuil = empHead.fd.HeadCuil;
        empRun.fd.HeadSalary = empHead.fd.HeadSalary;
        empRun.fd.Headcct = empHead.fd.Headcct;        
        empRun.fd.HeadRoster = empHead.fd.HeadRoster;
        empRun.fd.HeadCondition = empHead.fd.HeadCondition;
        empRun.fd.HeadStartDate = empHead.fd.HeadStartDate;
        empRun.fd.HeadFinishDate = empHead.fd.HeadFinishDate;
        empRun.fd.HeadAntiquity = empHead.fd.HeadAntiquity;

        //empRun.fd.HeadDiasTrab = empHead.fd.HeadDiasTrab;
        empRun.fd.HeadJournalType = empHead.fd.HeadJournalType;
        empRun.fd.HeadModcontract = empHead.fd.HeadModcontract;
        
        empRun.fd.HeadSitRevision = empHead.fd.HeadSitRevision;

        
    });

} 

     //CARGAR TABLA DE CORRIDA ABIERTA SEGUN EMPLEADO
     function LoadTablePayrunClosedByEmployee()
     {   

      var table = $("#TablePayrunClosedByEmployee").DataTable({
        //COMPROBACION PARA PINTAR Y CAMBIAR TEXTO DE TABLA
        "destroy": true,
        "scrollY":        "400px",
        "scrollX": "1100px",

        "language": {
            "url": "/../Spanish.json"
        },
        //Especificaciones de las Columnas que vienen y deben mostrarse
        "columns" : [
        { data : 'idn' },
        { data : 'nametyperun'},
        { data : 'nameroster'},
        { data : 'titleperiod'},
        { data : 'payments' },
        { data : 'unremuns' },
        { data : 'deductions' },
        { data : 'totalpay' }


        ],
        //Especificaciones de la URL del servicio
        "ajax": {
            url: '../api/v1/employee/payrunhead/close/'+empDP.fd.Idn,
            dataSrc : ''
        }
    });
      $('#TablePayrunClosedByEmployee tbody').on( 'click', 'tr', function () {

        var row = table.row( this ).data();
        empRun.fd.Idn = row.idn;
        
        empRun.openpayrundetail();
        empRun.fd.DetPayrunHead = row.idn;
        empRun.fd.DetTyperun = row.idntyperun;
        empRun.fd.DetPeriod = row.idnperiod;
        empRun.fd.DetEmployee = row.idnemployee;
        empRun.fd.DetRoster = row.idnroster;
        empRun.fd.DetPaymentmethod = row.idnpaymentmethod;
        empRun.fd.DetPaymentRundate = row.paydate;
        empRun.fd.DetPayContributiondate = row.paycontributiondate;
        console.log('Selected - Payrunhead:'+empRun.fd.DetPayrunHead);
        console.log('Selected - Typerun:'+empRun.fd.DetTyperun);
        console.log('Selected - Period:'+empRun.fd.DetPeriod);
        console.log('Selected - Employee:'+empRun.fd.DetEmployee);
        console.log('Selected - Roster:'+empRun.fd.DetRoster);
        console.log('Selected - Payment Method:'+empRun.fd.DetPaymentmethod);
        console.log('Selected - Payment RunDate:'+empRun.fd.DetPaymentRundate);
        console.log('Selected - Contribution Date:'+empRun.fd.DetPayContributiondate);
        //TRANSFERENCIA DE DATOS HACIA LA MODAL DE DETALLE
        empRun.fd.HeadCod = empHead.fd.HeadCod;
        empRun.fd.HeadName = empHead.fd.HeadName;
        empRun.fd.HeadCuil = empHead.fd.HeadCuil;
        empRun.fd.HeadSalary = empHead.fd.HeadSalary;
        empRun.fd.Headcct = empHead.fd.Headcct;        
        empRun.fd.HeadRoster = empHead.fd.HeadRoster;
        empRun.fd.HeadCondition = empHead.fd.HeadCondition;
        empRun.fd.HeadStartDate = empHead.fd.HeadStartDate;
        empRun.fd.HeadFinishDate = empHead.fd.HeadFinishDate;
        empRun.fd.HeadAntiquity = empHead.fd.HeadAntiquity;

        //empRun.fd.HeadDiasTrab = empHead.fd.HeadDiasTrab;
        empRun.fd.HeadJournalType = empHead.fd.HeadJournalType;
        empRun.fd.HeadModcontract = empHead.fd.HeadModcontract;
        
        empRun.fd.HeadSitRevision = empHead.fd.HeadSitRevision;

        
    });

  } 

function getactualvalue(list,idn)
{

 var result = list[parseInt(idn) - 1];
 return result;
}
