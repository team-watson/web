Vue.http.headers.common['X-CSRF-TOKEN'] = $('meta[name="csrf-token"]').attr('content');

//FUNCIONES GENERICAS PARA INICIO DE LA PAGINA
 var vm = new Vue({
  methods: 
      {
        loadcompany:function()
        {
       var query = window.location;
    var qs = getUrlParams(query);
    console.log("PARAMETROS",qs);

        },
          
           loadUser:function()
        {
                   // GET /someUrl
            this.$http.get('/api/v1/user').then(response => {
                mod.fd.Idn = response.body.idn;
                mod.fd.Username = response.body.username;
                mod.fd.Rol = response.body.nombrerol;
                mod.fd.IdnRol = response.body.idnrol;
                mod.fd.AvatarImg = response.body.avatar === null || response.body.avatar === 'null' ? '':'/uploads/'+response.body.avatar;
          }, response => {
            // error callback
          });
        },
       
      }
    });

vm.loadUser();


//FUNCIONES EN LA MODAL DE EDICION
var mod = new Vue({
  el: '#vuedata',
  data: {
    globalurl:'../api/v1/user',
    fd:
    {
      Idn:'',
      Username:'',
      Rol:'',
        IdnRol:'',
      Password:'',
        Password1:'',
        Avatar:'',
        AvatarImg:''
   

    },
    ff:
    {
      save:false,
      edit:true,
    }    
    
  },
  methods: {
  
    editar: function (event) 
    {
         if(mod.fd.Password1 !== "" && mod.fd.Password !== ""){
             if(mod.fd.Password  !== mod.fd.Password1 )
             {
                 swal('los Passwords deben coincidir','','error');
                 return false;
             }
         }

       swal({
                title: "Confirmación de editado",
                text: "Estas seguro de editar este elemento",
                type: "warning",
                showCancelButton: true,
                confirmButtonText: 'Aceptar',
                cancelButtonText: 'Cancelar'
            }).then(function () {
                
          
                var arr = {
                    //DATOS DE FORMULARIO
                    username:mod.fd.Username,
                    idnrol:mod.fd.IdnRol,
                    password:mod.fd.Password,
                    avatar:mod.fd.Avatar
                                      
                  };
                  //VUE POST
                Vue.http.put(mod.globalurl+"/update/"+mod.fd.Idn,arr).then(response => {
                    // Si todo sale correcto              
                    swal('Modificada Correctamente','','success');

                   if(response.body.avatar === 'null'){
                   }else{
                       $('#AvatarUser').attr('src','../uploads/'+response.body.avatar);
                   }
                    mod.cleanform();
                    
                    },
                    
                                                                           
                    response => {
                      //Si no sale correcto
                            swal('Ocurrio un error al modificar','','error')
                            mod.cleanform();
                        });
                        
                    })
                    //FIN VUE POST
    },
      cleanform:function()
      {
          mod.fd.Password="";
          mod.fd.Password1="";
      }
  }
});

function getBase64(file) {
    var reader = new FileReader();
    reader.readAsDataURL(file);
    reader.onload = function () {
    mod.fd.Avatar = reader.result;
    $('#ImgAvatar').attr('src',reader.result).show();
    $('#FakeAvatar').hide();

    };
    reader.onerror = function (error) {
        console.log('Error: ', error);
    };
}

$(document).on('change','#AvatarInput',function () {

    var files = document.getElementById('AvatarInput').files;
    if (files.length > 0) {
        getBase64(files[0]);
    }
});

  



