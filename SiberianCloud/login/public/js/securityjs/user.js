Vue.http.headers.common['X-CSRF-TOKEN'] = $('meta[name="csrf-token"]').attr('content');

//FUNCIONES GENERICAS PARA INICIO DE LA PAGINA
 var vm = new Vue({
  methods: 
      {
        cargarcomborol:function()
        {
                   // GET /someUrl
            this.$http.get('../api/v1/rol').then(response => {

            // get body data
            modedit.Roloptions = response.body;

          }, response => {
            // error callback
          });
        },
        cargartabla:function()
        {
          CargarTabla();
        }
      }
    });

vm.cargarcomborol();
vm.cargartabla();

//FUNCIONES EN LA MODAL DE EDICION
var modedit = new Vue({
  el: '#vuedata',
  data: { 
  globalurl:'../api/v1/user',
    Idn:'',
    Username:'',
    Namerol:'',
    Rol:'',
    Roloptions: '',
    Password:'',
    Password2:'',
 
    save:false,
    edit:true,
    
   
    
  },
  methods: {
    guardar: function (event) 
    {
        var items = [modedit.Rol,modedit.Password,modedit.Username];
        var strings = ['Rol', 'Contraseña','Nombre de Usuario'];
        var checked = CheckNulls(items, strings);
        if (modedit.Password !== modedit.Password2) {
            swal('Las contraseñas no coinciden','','error');
            return false;
        }
        if(checked !== 0){
       swal({
        title: "Confirmación de guardado",
        text: "Estas seguro de guardar este elemento",
        type: "warning",
        showCancelButton: true,       
        confirmButtonText: 'Aceptar',
        cancelButtonText: 'Cancelar'
      }).then(function () {
       
       
         var arr = {username:modedit.Username,idnrol:modedit.Rol,password:modedit.Password};
               Vue.http.post(modedit.globalurl+"/save",arr).then(response => {
                // success callback
                swal('Guardado Correctamente','','success');
                modedit.cleanform();
     
              }, response => {
                  swal('Ocurrio un error al guardar','','error')
                cleanform();
              });
      })
     }
    },
    editar: function (event) 
    {

        var items = [modedit.Rol,modedit.Password,modedit.Username];
        var strings = ['Rol', 'Contraseña','Nombre de Usuario'];
        var checked = CheckNulls(items, strings);
        if (modedit.Password !== modedit.Password2) {
            swal('Las contraseñas no coinciden','','error');
            return false;
        }
        if(checked !== 0){
            swal({
        title: "Confirmar edición",
        text: "Estas seguro de editar este elemento",
        type: "warning",
        showCancelButton: true,       
        confirmButtonText: 'Aceptar',
        cancelButtonText: 'Cancelar'
      }).then(function () {
       
       
         var arr = {username:modedit.Username,idnrol:modedit.Rol};
               Vue.http.put(modedit.globalurl+"/update/"+modedit.Idn,arr).then(response => {
                // success callback
                swal(
                    'Editado Correctamente','','success');
                modedit.cleanform();
     
              }, response => {
                  swal('Ocurrio un error al editar','','error');
                // error callback
              });
      })
            }
    },
    borrar: function (event)
    {
      swal({
        title: "Confirmar Eliminación",
        text: "Estas seguro de eliminar este elemento",
        type: "warning",
        showCancelButton: true,       
        confirmButtonText: 'Aceptar',
        cancelButtonText: 'Cancelar'
      }).then(function () {
       
       
         var arr = {username:modedit.Username};
                Vue.http.put(modedit.globalurl+"/delete/"+modedit.Idn,arr).then(response => {
                // success callback
                 swal('Eliminado Correctamente','','success');
                modedit.cleanform();
                  
     
              }, response => {
                swal('Ocurrio un error al eliminar','','error');
                // error callback
              });
      })
    },
    actualizarpassword: function(event)
    {
       if (modedit.Password !== modedit.Password2)
      {
        swal('Las contraseñas no coinciden','','error');
        return false;
      }
      swal({
        title: "Confirmar Actualización",
        text: "Estas seguro de actualizar la contraseña",
        type: "warning",
        showCancelButton: true,       
        confirmButtonText: 'Aceptar',
        cancelButtonText: 'Cancelar'
      }).then(function () {
       
       
         var arr = {username:modedit.Username,password:modedit.Password};
                 Vue.http.put(modedit.globalurl+"/updatepass/"+modedit.Idn,arr).then(response => {
                // success callback
                swal(
                    'Actualizado Correctamente','','success');
                modedit.cleanform();
     
              }, response => {
                  swal('Ocurrio un error al editar','','error');
                // error callback
              });
      })
    },
     openmodaltonew:function()
        {
          $('#ModalEdicion').modal('toggle');
          //OCULTAR BOTONES O MOSTRAR
          modedit.clean();
          modedit.save = true;
          modedit.edit = false;
        },
        openmodaltoedit:function()
        {
           $('#ModalEdicion').modal('toggle');
          //OCULTAR BOTONES O MOSTRAR
          modedit.save = false;
          modedit.edit = true;
        },

         cleanform:function()
{
      modedit.Idn='';
      modedit.Username='';
      modedit.Rol='';
  $('#ModalEdicion').modal('hide');
  var table = $('#TablaUsuarios').dataTable();
  //RECARGA LOS DATOS DE LA TABLA
 table.fnReloadAjax();

},


clean:function()
{
   modedit.Idn='';
      modedit.Username='';
      modedit.Rol='';
}

}
 



});

    //======INICIO DE FUNCIONES ============

    //======INICIO DE FUNCIONES ============
    function CargarTabla()
    {
     var table = $("#TablaUsuarios").DataTable({
          //COMPROBACION PARA PINTAR Y CAMBIAR TEXTO DE TABLA

          //Especificacion de Ancho y Alto de la tabla
          "rowCallback": function( row, data, index ) {
           if ( data.Activo == "1" ) 
           {
            $('td:eq(4)', row).html( '<b>Activo</b>' );
            $('td:eq(4)', row).css('background-color', '#98FEE6');
          }
          else
          {
            $('td:eq(4)', row).html( '<b>Inactivo</b>' );
            $('td:eq(4)', row).css('background-color', '#FEE698');
          }
          $('td:eq(0)', row).css('background-color', '#ECECEC');
        },
        // "destroy": true,
        "scrollY":        "200px",
        "scrollX":        "1000px",
       //Especificaciones de las Columnas que vienen y deben mostrarse
       "columns" : [
       { data : 'idn' },
       { data : 'username' },
       { data : 'nombrerol' },

       
       ],    
       //Especificaciones de la URL del servicio
       "ajax": {
        url: "../api/v1/userall",
        dataSrc : ''
      }

    });  
       //Al Hacer clic en la tabla, obtengo los datos y los cargo a los TextBox
       $('#TablaUsuarios tbody').on( 'click', 'tr', function () {

        var Activo = table.row( this ).data().Activo;

        $('html, body').animate({ scrollTop: 0 }, 'slow', function () {});
        //OBTENGO LOS VARLOES DE LA TABLA
        var idn = table.row( this ).data().idn;
        var username = table.row( this ).data().username;
        var idnrol = table.row( this ).data().idnrol;
       
        //ASIGNO LOS VALORES A LA 
        modedit.Idn = idn;
        modedit.Username = username;
        modedit.Rol = idnrol;
      
      //ABRO LA MODAL
        modedit.openmodaltoedit();
 } );

     } 