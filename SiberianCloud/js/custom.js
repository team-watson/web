jQuery(document).ready(function($) {

	

	$('.scrollup').click(function(){
		$("html, body").animate({ scrollTop: 0 }, 1000);
		return false;
	});
	
		$('.accordionn').on('show', function () {
			
			//active: true,
			//collapsible: true
			//$(e.target).prev('.accordion-heading').find('.accordion-toggle').addClass('active');
			//$(e.target).prev('.accordion-heading').find('.accordion-toggle i').removeClass('icon-plus');
			//$(e.target).prev('.accordion-heading').find('.accordion-toggle i').addClass('icon-minus');
		});
		$( ".accordionn" ).on("option", { 
			collapsible: true,
			display: false
		});
		
		/*$('.accordion').on('hide', function () {
			close();
			$(this).find('.accordion-toggle').not($(e.target)).removeClass('active');
			$(this).find('.accordion-toggle i').not($(e.target)).removeClass('icon-minus');
			$(this).find('.accordion-toggle i').not($(e.target)).addClass('icon-plus');
			
		});	*/
		
		$('.accordionn').on('click','h1',function(){
			
			var t = $(this);
			var tp = t.next();
			var p = t.parent().siblings().find('p');
			tp.slideToggle();
			p.slideDown();
		});


		(function($) {
			"use strict";
		
			// manual carousel controls
			$('.next').click(function(){ $('.carousel').carousel('next');return false; });
			$('.prev').click(function(){ $('.carousel').carousel('prev');return false; });
			
		})(jQuery);
		
		$('#myCarouselCustom').carousel({
			animation: "swing",
			direction: "horizontal", 
			slideshow: true,
			display: "flex",
			slideshowSpeed: 3500,
			animationDuration: 3000,
			interval: 6000,
			smootheHeight:true,	
			 pause:true
		});
		// Go to the previous item
		$("#prevBtn").click(function(){
			$("#myCarouselCustom").carousel("prev");
		});
		// Go to the previous item
		$("#nextBtn").click(function(){
			$("#myCarouselCustom").carousel("next");
		});
	
	// Activate Carousel
	$("#myCarousel").carousel();
	
	// Enable Carousel Indicators
	$(".item").click(function(){
	  $("#myCarousel").carousel(1);
	});
	
	// Enable Carousel Controls
	$(".left").click(function(){
	  $("#myCarousel").carousel("prev");
	});
	

	/*$('.navigation').onePageNav({
		begin: function() {
			console.log('start');
		},
		end: function() {
			console.log('stop');
		},
			scrollOffset: 0		
	});*/
	
	// prettyPhoto
	$("a[data-pretty^='prettyPhoto']").prettyPhoto();		

    // Localscrolling 
	//$('#menu-main, .brand').localScroll();
	
	$('#menu-main li a').click(function(){
		var links = $('#menu-main li a');
		links.removeClass('selected');
		$(this).addClass('selected');
	});

    var iOS = false,
        p = navigator.platform;

    if (p === 'iPad' || p === 'iPhone' || p === 'iPod') {
        iOS = true;
    }	
	
    if (iOS === false) {

        $('.flyIn').bind('inview', function (event, visible) {
            if (visible === true) {
                $(this).addClass('animated fadeInUp');
            }
        });

        $('.flyLeft').bind('inview', function (event, visible) {
            if (visible === true) {
                $(this).addClass('animated fadeInLeftBig');
            }
        });

        $('.flyRight').bind('inview', function (event, visible) {
            if (visible === true) {
                $(this).addClass('animated fadeInRightBig');
            }
        });

    }
	
	// add animation on hover
		$(".service-box").hover(
			function () {
			$(this).find('img').addClass("animated pulse");
			$(this).find('h2').addClass("animated fadeInUp");
			},
			function () {
			$(this).find('img').removeClass("animated pulse");
			$(this).find('h2').removeClass("animated fadeInUp");
			}
		);
		
	
	// cache container
	var $container = $('#portfolio-wrap');
	/*$.browser.safari = ($.browser.webkit && !(/chrome/.test(navigator.userAgent.toLowerCase())));	
	
	if($.browser.safari){ 	
	// initialize isotope
	$container.isotope({
		animationEngine : 'jquery',
		animationOptions: {
			duration: 200,
			queue: false
		},
		layoutMode: 'fitRows'
	});
	} else {	
	$container.isotope({
		animationEngine : 'best-available',
		animationOptions: {
			duration: 200,
			queue: false
		},
		layoutMode: 'fitRows'
	});	*/
	
	$(window).resize(function() {
		$container.isotope('reLayout');
	});
	
	// filter items when filter link is clicked
	$('#filters a').click(function(){
		$('#filters a').removeClass('active');
		$(this).addClass('active');
		var selector = $(this).attr('data-filter');
		$container.isotope({ filter: selector });
		return false;
	});

	// flexslider main
	$('#main-flexslider').flexslider({						
		animation: "swing",
		direction: "horizontal", 
		slideshow: true,
		slideshowSpeed: 3500,
		animationDuration: 1000,
		smootheHeight:true,						
		useCSS: false
	});
});


	
	

