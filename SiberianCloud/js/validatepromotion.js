$(document).on("ready", catalogue);

    
 function catalogue() {
     $.getJSON("https://siberian-api.herokuapp.com/api/catalogue",
         function (datos) {
     var html = "";

     $.each(datos, function(i, item) {
       if (i == "status" || i == "message") {
         html += "<div>   </div>";
       } else {
         var categories = [];
         categories = item;
         $.each(categories, function(i, item) {
           var name = item.name;
           var description = item.description;
           var img = item.imageUrl;

           if (name == undefined || description == undefined) {
             html += "<div> no hay datos  </div>";
           } else {
             var sub = [];
             sub = item.subcategories;

             $.each(sub, function(i, item) {
               var subname = item.name;
               var subdescription = item.description;
               var id = item.id;

               if (id != 1) {
                 html += "<div class='carousel-item col-md-4'>";
                 html += "<div class='card'>";
                 html +=
                   "<img class='card-img-top img-fluid' src='" +
                   img +
                   "' alt='Card image cap'>";
                 html += "<div class='card-body'>";
                 html += "<h4 class='card-title'>" + subname + "</h4>";
                 html += "<p class='card-text'>" + subdescription + "</p>";
                 html +=
                   "<a href='login/resources/views/Index.html'><button class='site-btn'>Solicitar</button></a>";
                 html += "</div>";
                 html += "</div>";
                 html += "</div>";
               } else {
                 html += "<div class='carousel-item col-md-4 active'>";
                 html += "<div class='card'>";
                 html +=
                   "<img class='card-img-top img-fluid' src='" +
                   img +
                   "' alt='Card image cap'>";
                 html += "<div class='card-body'>";
                 html += "<h4 class='card-title'>" + subname + "</h4>";
                 html += "<p class='card-text'>" + subdescription + "</p>";
                 html +=
                   "<a href='login/resources/views/Index.html'><button class='site-btn'>Solicitar</button></a>";
                 html += "</div>";
                 html += "</div>";
                 html += "</div>";
               }
             });
           }
         });
       }
     });

     $("#carouselcardtest").html(html);
   });
 }

    

