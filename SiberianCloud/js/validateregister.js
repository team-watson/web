$(document).on("ready", main);

function main() {
  // http://192.168.0.102:12225/api/company
  var logo = "";
  $.getJSON("https://siberian-api.herokuapp.com/api/company", function(datos) {
    
    if (datos == undefined || datos == null || datos == "") {
      //window.location = "errorMessage.html";
      console.log("No hay logo disponible o no hay una empresa registrada")
    } else {
      $.each(datos, function(i, item) {
       
        logo = item.imageUrl;

      

        $("img").each(function() {
          var curSrc = $(this).attr("src");
          if (curSrc === "") {
            $(this).attr("src", logo);
          }
        });

      
      });
    }
  });
}
